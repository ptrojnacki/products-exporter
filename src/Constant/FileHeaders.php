<?php

namespace WPDesk\ProductsExporter\Constant;

/**
 * Headers for the generated files
 */
class FileHeaders {
	/**
	 * Headers for generated file with products data
	 *
	 * @return string[]
	 */
	public static function products_csv_headers() : array {
		return [ 'nazwa', 'sku', 'cena_regularna', 'cena_promocyjna', 'kategorie' ];
	}
}
