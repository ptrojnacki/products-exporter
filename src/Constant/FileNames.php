<?php

namespace WPDesk\ProductsExporter\Constant;

/**
 * Names for the generated files
 */
class FileNames {
	public const PRODUCTS = 'produkty.csv';
}
