<?php

namespace WPDesk\ProductsExporter\Controller;

/**
 * Abstract class for controllers
 */
abstract class AbstractController {
	/**
	 * Get the bas path of plugin templates folder
	 *
	 * @return string
	 */
	protected function get_templates_path(): string {
		return plugin_dir_path( dirname( __DIR__ ) ) . '/templates';
	}
}
