<?php

namespace WPDesk\ProductsExporter\Controller;

use WPDesk\ProductsExporter\Constant\FileHeaders;
use WPDesk\ProductsExporter\Constant\FileNames;
use WPDesk\ProductsExporter\Service\CsvGenerator;
use WPDesk\ProductsExporter\Provider\ProductsProvider;

/**
 * Class responsible for rendering templates and handling requests
 */
class ProductsExporterController extends AbstractController {

	/**
	 * Products data provider
	 *
	 * @var ProductsProvider
	 */
	private $products_provider;

	/**
	 * Csv file generator
	 *
	 * @var CsvGenerator
	 */
	private $csv_generator;

	public function __construct() {
		$this->products_provider = new ProductsProvider();
		$this->csv_generator     = new CsvGenerator();
	}

	/**
	 * Render page template
	 *
	 * @internal
	 *
	 * @return void
	 */
	public function show() : void {
		require_once $this->get_templates_path() . '/products-export/show.php';
	}

	/**
	 * Download generated file with products data
	 *
	 * @internal
	 *
	 * @return void
	 */
	public function download() : void {
		if ( ! isset( $_POST['products-exporter-download-csv-nonce'] ) ) {
			wp_die( esc_html__( 'Nie kombinuj', 'products-exporter' ) );
		}

		$nonce = sanitize_text_field( wp_unslash( $_POST['products-exporter-download-csv-nonce'] ) );

		if ( ! wp_verify_nonce( $nonce, 'products-exporter-download-csv' ) ) {
			wp_die( esc_html__( 'Nie kombinuj', 'products-exporter' ) );
		}

		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( esc_html__( 'Nie masz dostępu', 'products-exporter' ) );
		}

		$products_data = $this->products_provider->get_products_data();
		$this->csv_generator->generate( FileHeaders::products_csv_headers(), $products_data, FileNames::PRODUCTS );
	}
}
