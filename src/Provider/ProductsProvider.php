<?php

namespace WPDesk\ProductsExporter\Provider;

/**
 * Class responsible for getting products
 */
final class ProductsProvider {
	/**
	 * Get products data
	 *
	 * @return array<array<string>>
	 */
	public function get_products_data() : array {
		$args = [
			'limit'  => -1,
			'status' => 'publish',
		];

		$products = wc_get_products( $args );

		if ( ! is_array( $products ) || empty( $products ) ) {
			return [];
		}

		$data = [];

		foreach ( $products as $product ) {
			if ( ! $product instanceof \WC_Product ) {
				continue;
			}

			$categories_display = $this->get_categories_display( $product );

			$data[] = $this->transform_product( $product, $categories_display );

			if ( $product instanceof \WC_Product_Variable ) {
				$variations = $product->get_available_variations( 'objects' );

				foreach ( $variations as $variation ) {
					if ( ! $variation instanceof \WC_Product_Variation ) {
						continue;
					}
					$data[] = $this->transform_variation( $variation, $categories_display );
				}
			}
		}

		return $data;
	}

	/**
	 * Transform the product object into the array with data
	 *
	 * @param \WC_Product $product WooCommerce product.
	 * @param string      $categories_display Category names string.
	 *
	 * @return string[]
	 */
	private function transform_product( \WC_Product $product, string $categories_display ) : array {
		$product_data = [];

		$product_data['title']         = $product->get_title();
		$product_data['sku']           = $product->get_sku();
		$product_data['regular_price'] = $product->get_regular_price();
		$product_data['sale_price']    = $product->get_sale_price();
		$product_data['categories']    = $categories_display;

		return $product_data;
	}

	/**
	 * Transform the product variation object into the array with data
	 *
	 * @param \WC_Product_Variation $variation WooCommerce product variation.
	 * @param string                $categories_display Category names string.
	 *
	 * @return string[]
	 */
	private function transform_variation( \WC_Product_Variation $variation, string $categories_display ) : array {
		$product_data = [];

		$variation_data = $variation->get_data();

		$product_data['title']         = $variation_data['name'];
		$product_data['sku']           = $variation_data['sku'];
		$product_data['regular_price'] = $variation_data['regular_price'];
		$product_data['sale_price']    = $variation_data['sale_price'];
		$product_data['categories']    = $categories_display;

		return $product_data;
	}

	/**
	 * Get product category names as a string
	 *
	 * @param \WC_Product $product WooCommerce product.
	 *
	 * @return string
	 */
	private function get_categories_display( \WC_Product $product ) : string {
		$categories = get_the_terms( $product->get_id(), 'product_cat' );

		if ( ! is_array( $categories ) || empty( $categories ) ) {
			return '';
		}

		$category_names = array_map(
			function ( $category ) {
				return $category->name;
			},
			$categories
		);

		return implode( ', ', $category_names );
	}
}
