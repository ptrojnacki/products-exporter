<?php

namespace WPDesk\ProductsExporter;

use ProductsExporterVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\ProductsExporter\Controller\ProductsExporterController;

/**
 * Class responsible for setup hooks
 */
final class ProductsExporter implements Hookable {

	/**
	 * @var ProductsExporterController $products_exporter_controller
	 */
	private $products_exporter_controller;

	public function __construct() {
		$this->products_exporter_controller = new ProductsExporterController();
	}

	/**
	 * Init hooks
	 *
	 * @return void
	 */
	public function hooks() : void {
		add_action( 'admin_menu', [ $this, 'admin_submenu_page' ] );
		add_action( 'parse_request', [ $this, 'parse_request' ] );
	}

	/**
	 * Add admin submenu page
	 *
	 * @internal
	 *
	 * @return void
	 */
	public function admin_submenu_page() : void {
		add_submenu_page(
			'edit.php?post_type=product',
			__( 'Eksport produktów', 'products-exporter' ),
			__( 'Eksport produktów', 'products-exporter' ),
			'manage_options',
			'products-export.php',
			[ $this->products_exporter_controller, 'show' ]
		);
	}

	/**
	 * Add route for downloading file
	 *
	 * @internal
	 *
	 * @param \WP $wp WordPress environment setup class.
	 *
	 * @return void
	 */
	public function parse_request( \WP $wp ) : void {
		if ( 'products-exporter-download-csv' === $wp->request ) {
			$this->products_exporter_controller->download();
		}
	}
}
