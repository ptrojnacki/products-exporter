<?php

namespace WPDesk\ProductsExporter\Service;

use ProductsExporterVendor\League\Csv\Exception;
use ProductsExporterVendor\League\Csv\Writer;

require_once dirname( dirname( __DIR__ ) ) . '/vendor_prefixed/league/csv/src/functions_include.php';

/**
 * Class responsible for generating a csv file
 */
final class CsvGenerator {
	/**
	 * Generate a csv file
	 *
	 * @param string[]             $headers File headers list.
	 * @param array<array<string>> $data Data to insert.
	 * @param string               $file_name Name of the generated file.
	 *
	 * @return void
	 */
	public function generate( array $headers, array $data, string $file_name ) : void {
		try {
			$csv = Writer::createFromString( '' );
			$csv->setDelimiter( ';' );
			$csv->setNewline( "\r\n" );
			$csv->setOutputBOM( Writer::BOM_UTF8 );
			$csv->insertOne( $headers );
			$csv->insertAll( $data );
			$csv->output( $file_name );
		} catch ( Exception $e ) {
			wp_die( esc_html__( 'Wystąpił błąd przy generowaniu pliku', 'products-exporter' ) );
		}

		exit();
	}

	/**
	 * Headers for generated file with products data
	 *
	 * @return string[]
	 */
	public function product_data_headers() : array {
		return [ 'nazwa', 'sku', 'cena_regularna', 'cena_promocyjna', 'kategorie' ];
	}
}
