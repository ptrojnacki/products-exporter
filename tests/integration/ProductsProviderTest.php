<?php

namespace integration;

use PHPUnit\Framework\TestCase;
use WPDesk\ProductsExporter\Provider\ProductsProvider;


class ProductsProviderTest extends TestCase {

	/**
	 * @var ProductsProvider $products_provider
	 */
	private $products_provider;

	protected function setUp() {
		$this->products_provider = new ProductsProvider();
	}

	protected function tearDown() {
		$this->clearDatabase();
	}

	public function testShouldGetSimpleProductData() {
		$category1 = wp_insert_term( 'Category 1', 'product_cat', [] );
		$category2 = wp_insert_term( 'Category 2', 'product_cat', [] );
		$category_ids = [ $category1['term_id'], $category2['term_id'] ];

		$simple_product = new \WC_Product_Simple();
		$simple_product->set_name( 'Dummy Simple Product' );
		$simple_product->set_sku( 'DUMMY SKU' );
		$simple_product->set_regular_price( '100' );
		$simple_product->set_sale_price( '90' );
		$simple_product->set_category_ids( $category_ids );

		$simple_product->save();

		$data = $this->products_provider->get_products_data();

		$expected = [
			[
				'title' => 'Dummy Simple Product',
				'sku' => 'DUMMY SKU',
				'regular_price' => '100',
				'sale_price' => '90',
				'categories' => 'Category 1, Category 2',
			],
		];

		$this->assertSame( $expected, $data );
	}

	public function testShouldGetProductsWithVariations() : void {
		$product = new \WC_Product_Variable();

		$product->set_name( 'Dummy Variable Product' );

		$attribute = new \WC_Product_Attribute();
		$attribute->set_name( 'Color' );
		$attribute->set_options( [ 'Blue', 'Red' ] );
		$attribute->set_visible( true );
		$attribute->set_variation( true );

		$product->set_attributes( [ $attribute ] );

		$product->save();

		$variation = new \WC_Product_Variation();
		$variation->set_parent_id( $product->get_id() );
		$variation->set_attributes( [ 'Color' => 'Blue' ] );
		$variation->set_regular_price( '300' );
		$variation->save();

		$variation = new \WC_Product_Variation();
		$variation->set_parent_id( $product->get_id() );
		$variation->set_attributes( [ 'Color' => 'Red' ] );
		$variation->set_regular_price( '500' );
		$variation->save();

		$data = $this->products_provider->get_products_data();

		$this->assertCount(3, $data);
	}

	private function clearDatabase() : void {
		$products = wc_get_products( [ 'limit' => -1 ] );

		/** @var \WC_Product $product */
		foreach ( $products as $product ) {
			$product->delete( true );
		}

		$product_categories = get_terms( [ 'taxonomy' => 'product_cat', 'hide_empty' => false ] );

		/** @var \WP_Term $category */
		foreach ( $product_categories as $category ) {
			wp_delete_term( $category->term_id, 'product_cat' );
		}
	}
}
