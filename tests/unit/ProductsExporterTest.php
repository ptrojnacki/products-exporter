<?php

namespace unit;

use ProductsExporterVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\ProductsExporter\ProductsExporter;
use WP_Mock\Tools\TestCase;

class ProductsExporterTest extends TestCase {

	/**
	 * @var ProductsExporter
	 */
	private $products_exporter;

	public function setUp() : void {
		\WP_Mock::setUp();
		$this->products_exporter = new ProductsExporter();
	}

	public function tearDown() : void {
		\WP_Mock::tearDown();
	}

	public function testShouldImplementsHookable() : void {
		$this->assertInstanceOf( Hookable::class, $this->products_exporter );
	}

	public function testShouldAddHooks() : void {
		\WP_Mock::expectActionAdded( 'admin_menu', [ $this->products_exporter, 'admin_submenu_page' ] );
		\WP_Mock::expectActionAdded( 'parse_request', [ $this->products_exporter, 'parse_request' ] );

		$this->products_exporter->hooks();

		$this->assertTrue( true );
	}

	public function testShouldAddSubmenuPage() : void {
		\WP_Mock::userFunction( 'add_submenu_page' )
			->times( 1 );

		$this->products_exporter->admin_submenu_page();

		$this->assertTrue( true );
	}
}
