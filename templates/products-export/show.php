<?php
/**
 * Admin products export page template
 */

?>
<div class="wrap">
	<h1 class="wp-heading-inline"><?php esc_html_e( 'Eksport produktów', 'products-exporter' ); ?></h1>
</div>
<form method="POST" action="/products-exporter-download-csv">
	<?php wp_nonce_field( 'products-exporter-download-csv', 'products-exporter-download-csv-nonce' ); ?>
	<button type="submit" class="button button-primary"><?php esc_html_e( 'Wygeneruj csv', 'products-exporter' ); ?></button>
</form>
